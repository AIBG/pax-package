### Kako pruzeti osnovni paket:

#### Prvi način(ssh)
```
git clone --recursive git@gitlab.com:AIBG/pax-package.git
```

#### Drugi način(uz vaš gitlab password)
```
git clone --recursive https://gitlab.com/AIBG/pax-package.git
```

#### Treći način(download .zip fajla)
samo downloadajte .zip file, međutim, ako mislite korisiti c++ primjer bota upišite:
```
git submodule init
```

### Koristenje platforme

* ```ai-server.jar <config file>``` ==> glavni server koji pokrece cijelu igru
* ```ai-historic-grid.jar <config file>``` ==> isto kao server ali cijeli igru odsimulira te pomocu ```J``` i ```K``` tipki idete potrez naprijed nazad. Drugom bojom su oznacene selektirate akcije za taj potez
* ```ai-server-testgrid.jar <config file>``` ==> Ovdje mozete sami isprobavati potez po potez. Akcije prvog igraca selektirate lijevom tipkom misa, akcije drugoga desnom tipkom misa dok srednjom ponistavate akcije. Pritisnite ```ENTER``` za simulaciju sljedeceg poteza

## Pravila igre:

Originalna verzija igre Game of life:

(Pravila kopirana s wikipedije, guglati "game of life")

##### 1.Any live cell with fewer than two live neighbours dies, as if caused by under-population.
##### 2.Any live cell with two or three live neighbours lives on to the next generation.
##### 3.Any live cell with more than three live neighbours dies, as if by over-population.
##### 4.Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.

Ta četiri pravila vrijede i u našoj verziji igre osim što se broj
susjeda ćelije računa se kao broj prijateljskih aktivnih susjeda - broj neprijateljskih aktivnih susjeda.


Igri prisustvuju dva bota. Polje je veličine 24x24 i oblika je torusa (lijeva strana se nastavlja na desnu i gornja na donju).
Prije računanja novog stanja botovi dodaju na polje određeni broj akcija (aktiviraju one ćelije koje žele).
Postoji limit na udaljenosti na kojoj se ćelije mogu aktivirati. Bot (vaš ili protivnički) može aktivirati novu ćeliju samo ako je ona, po infinum normi, udaljena najviše _dva_ od najbliže ćelije koja pripada tom botu.
Ćelija ne može biti aktivirana ako već pripada protivničkom botu



Rad botova:
Za izradu botova možete koristiti C++, python i javu (ovdje imate primjere)

### Json koji se šalje igračima u svakoj iteraciji(potezu) sadrži sljedeće elemente:

##### "field":
array od 24 elementa kojem je svaka string duljine 24 (personalizirano je botovima tako da znak '#' označava prijateljsku čeliju, '0' neprijateljsku, a '.' neaktivnu čeliju)
##### "cellsRemaining":
koliko možete čelija aktivirati u ovom potezu (neće preći maxCellCapacity)
##### "cellGainPerTurn":
koliko čelija ćete dobiti svaki potez za aktiviranje
##### "maxCellCapacity":
koliko čelija možete maksimalno imati skladišteno( npr. odlučite štediti čelije ovaj potez i imati više čelija za sljedeći)
##### "maxColonisationDistance":
koliko daleko od neke prijateljske aktivne čelije možete aktvirati svoju (bit će 2, u normi beskonačno, što znači kvadrat 5x5 oko bilo koje aktivne prijateljske čelije)
##### "currIteration":
trenutna iteracija(počinje od nule)
##### "maxGameIterations":
uvijek 500
##### "timeGainPerTurn":
koliko svaki potez dobijete vremena (bit će 300 ms)
##### "timeLeftForMove":
koliko ovaj potez imate vremena (300ms, na početku imate više jer znamo da jvm-u treba neko vrijem da se "zagrije")

### Json koji šaljete igri:

lista ćelija koje želite aktivirati s nazivom "cells"
npr:
{"cells": [[10, 20], [10, 18], [12, 15], [10, 16], [12, 20]]}


# Važno!!

Botovi su samo pokazni i njhovi algoritmi ne garantiraju tocnost

### java bot krivo računa udaljenost(koristi manhattan umjesto infinity norme)
### c++ bot vjerojatno isto nešto od toga :D
